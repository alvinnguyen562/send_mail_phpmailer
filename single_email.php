<?php

$subject = $_REQUEST['subject'];
$email = $_REQUEST['email'];
$message = $_REQUEST['message'];

// here we use the php mail function
// to send an email to:
// you@example.com
// mail("bradm@inmotiontesting.com", "Feedback Form Results", $message, "From: $email");

require "lib/class.phpmailer.php";
require 'lib/PHPMailerAutoload.php';

$mail = new PHPMailer(); // create a new object
$mail->CharSet = 'UTF-8';
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
$mail->Host = "smtp.gmail.com";
$mail->Port = 465; // or 587
$mail->IsHTML(true);
$mail->Username = "nguyenminhtuan.dev@gmail.com";
$mail->Password = "";
$mail->SetFrom("nguyenminhtuan.dev@gmail.com");
$mail->Subject = $subject;
$mail->Body = $message;
$mail->AddAddress($email);

if (!$mail->Send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Successfull!";
}
